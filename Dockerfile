FROM node:14-alpine
WORKDIR /app
COPY yarn.lock package.json ./
RUN yarn
COPY . .
CMD ["node", "/app/index.js"]